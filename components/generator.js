import React from 'react'
import { randomTitle } from '../utils/generate'
import styles from '../styles/Generator.module.css'

export default class Generator extends React.Component {
  constructor() {
    super()
      this.state = {
      generated: ""
    }
  }

  render() {
    const generatedText = this.state.generated

    return (
      <div className={styles.generate}>
        <div>
          <button className={styles.button} onClick={() => this.setState({generated: randomTitle(1, 3, 2, 10)})}>
            Generate now!
          </button>
        </div>
        <div>
          {generatedText.length > 0 ? <h2 className={styles.result}>{generatedText}</h2> : <></>}
        </div>
      </div>
    )
  }
}
