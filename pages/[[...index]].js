import Head from 'next/head'
import Image from 'next/image'
import Generator from "../components/generator"
import styles from '../styles/Home.module.css'

const title = "Kajgūn title generator"

export default function Home() {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/images/favicon.ico" />
      </Head>
      <main className={styles.container}>
        <Image
          className={styles.rounded}
          src="/images/logo.jpg"
          height="100"
          width="100"
          alt="Kajgūn logo"
        />
        <h1>{`Welcome to the ${title}!`}</h1>
        <a href="https://linktr.ee/kajgun" target="_blank" rel="noopener noreferrer">What is Kajgūn?</a>
        <Generator />
      </main>
    </div>
  )
}
