import { render, act } from '@testing-library/react'
import Home from '../pages/[[...index]]'
import '@testing-library/jest-dom'

describe('Home', () => {
  it('renders a heading', () => {
    const { container } = render(<Home />)

    const h2s = container.querySelectorAll("h2")
    expect(h2s.length).toBe(0)

    act (() => {
      const startButton = container.querySelectorAll("button")[0]
      startButton.dispatchEvent(new MouseEvent("click", { bubbles: true })) 
    })

    const h2s2 = container.querySelectorAll("h2")
    expect(h2s2.length).toBe(1)
  })
})