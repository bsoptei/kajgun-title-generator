import { randomTitle } from "../utils/generate.js"

it("random title should be random but with given constraints", () => {
  const minWords = 1
  const maxWords = 4
  const minWordLength = 6
  const maxWordLength = 7
  const create = () => randomTitle(minWords, maxWords, minWordLength, maxWordLength).split(" ")
  const title = create()
  const titleLength = title.length

  expect(titleLength >= minWords && titleLength <= maxWords).toBe(true)

  const title2 = create()

  expect(title2).not.toBe(title)
})

