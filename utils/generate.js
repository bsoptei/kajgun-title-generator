const consonants = "bcdfghjklmnpqrstvwxyz"
const vowels = "aeiou"

export function randomTitle(minWords, maxWords, minWordLength, maxWordLength) {
  return [...Array(randomInt(minWords, maxWords)).keys()]
    .map(_ => randomWord(minWordLength, maxWordLength)).join(" ")
}

function randomInt(min, max) {
  return parseInt((Math.random() * (max - min + 1)), 10) + min
}

function randomWord(minWordLength, maxWordLength) {
  return [...Array(randomInt(minWordLength, maxWordLength)).keys()].map(_ => {
    let consonant = Math.random() > 0.5

    return consonant ? 
      consonants[randomInt(0, consonants.length - 1)] : vowels[randomInt(0, vowels.length - 1)]
  }).join("") 
}
